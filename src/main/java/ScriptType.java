public enum ScriptType {
    EMPLOYER_GENERATOR {
        ScriptGenerator generator = new EmployerScriptGenerator();
        ScriptGenerator getInstance(){
            return generator;
        }
    };
    abstract ScriptGenerator getInstance();
}
