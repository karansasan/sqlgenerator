import org.apache.poi.ss.usermodel.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class EmployerScriptGenerator implements ScriptGenerator{
    private static Map<String, String> employerDBMapper = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);;
    {
        employerDBMapper.put("Group Name", "employer_name");
        employerDBMapper.put("EMPLOYEES ", "form_number_of_employees");
        employerDBMapper.put("Group Policy #", "group_health_policy_number");
        employerDBMapper.put("Carrier ID", "carrier_id");
        employerDBMapper.put("Carrier", "current_carrier");
        employerDBMapper.put("WEB LANDING PAGE", "landing_page");
    }

    private String trimAndReplaceSingleQuote(Cell cell) {
        return String.valueOf(cell).trim().replace("'","\\'");
    }

    @Override
    public void writeScript(Workbook workbook, int startingRow) {
        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
        Iterator<Sheet> sheetIterator = workbook.sheetIterator();
        System.out.println("Retrieving Sheets using Iterator");
        while (sheetIterator.hasNext()) {
            Sheet sheet = sheetIterator.next();
            System.out.println("=> " + sheet.getSheetName());
            Iterator<Row> rowIterator = sheet.rowIterator();
            Row header = sheet.getRow(startingRow);
            while (rowIterator.hasNext() && startingRow > 0) {
                rowIterator.next();
                startingRow--;
            }
            Map<String, Integer> dbColumnNameToCellValueMap = new HashMap<>();
            header.forEach(column -> dbColumnNameToCellValueMap.put(employerDBMapper.get(String.valueOf(column)), column.getColumnIndex()));
            DataFormatter formatter = new DataFormatter();
            int count=0;
            //TODO Need to make the script neater.
            while(rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if(row.getCell(dbColumnNameToCellValueMap.get("employer_name")) != null) {
                    count++;
                    String employerName = String.valueOf(row.getCell(dbColumnNameToCellValueMap.get("employer_name"))).toUpperCase().replaceAll("\\s+", "_").replaceAll("[^a-zA-Z0-9_]", "");
                    String employerDisplayName = trimAndReplaceSingleQuote(row.getCell(dbColumnNameToCellValueMap.get("employer_name")));
                    System.out.println("insert into advisor (first_name, last_name, firm_name, when_created, when_modified, who_created, who_modified, version) \n" +
                            "values ('Susan', 'Babineau', 'Dezyne Benefits Inc', now(), now(), 'karanpreet.singh@pocketpills.com', 'karanpreet.singh@pocketpills.com', 1);\n");
                    System.out.println("insert into employer (employer_name, employer_display_name, process_type, send_email_to_employees, form_number_of_employees, group_health_policy_number, carrier_id, current_carrier, plan_type, landing_page, markup_enum, employer_status, \n" +
                            "when_created, when_modified, who_created, who_modified, version\n" +
                            ")\n" +
                            "values('" +
                            employerName + "','" +
                            employerDisplayName + "'," +
                            "'v2'," +
                            "0," + (row.getCell(dbColumnNameToCellValueMap.get("form_number_of_employees")).getCellType() == CellType.BLANK ? "null" :
                            formatter.formatCellValue(row.getCell(dbColumnNameToCellValueMap.get("form_number_of_employees")))) + "," +
                            formatter.formatCellValue(row.getCell(dbColumnNameToCellValueMap.get("group_health_policy_number"))) + "," +
                            formatter.formatCellValue(row.getCell(dbColumnNameToCellValueMap.get("carrier_id"))) + ",'" +
                            formatter.formatCellValue(row.getCell(dbColumnNameToCellValueMap.get("current_carrier"))) + "'," +
                            "'VOLUNTARY_PLAN','" +
                            trimAndReplaceSingleQuote(row.getCell(dbColumnNameToCellValueMap.get("landing_page"))) + "'," +
                            "'_70'," +
                            "'ONBOARDED'," +
                            "now(), now(), 'karanpreet.singh@pocketpills.com', 'karanpreet.singh@pocketpills.com', 1);"
                    );
                    System.out.println("insert into employer_advisor (employer_id, advisor_id) values ((select MAX(ID) from employer where employer_name='" + employerName + "'), (select MAX(ID) from advisor where first_name='Susan' and last_name='Babineau'));\n");
                    System.out.println("commit;");
                }
            }
            System.out.println("Count rows processed: " + count);
        }
    }

}
