import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;

public class Manager {

    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.out.println("Provided arguments should be of size 3");
            return;
        }

        ScriptType scriptType = ScriptType.valueOf(args[0]);
        String fileLocation = args[1];
        int startingRow = Integer.parseInt(args[2]);
        try (Workbook workbook = WorkbookFactory.create(new File(fileLocation))) {
            scriptType.getInstance().writeScript(workbook, startingRow);
        }
    }
}
