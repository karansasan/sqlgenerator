import org.apache.poi.ss.usermodel.Workbook;

public interface ScriptGenerator {
    void writeScript(Workbook workbook, int startingRow);
}
